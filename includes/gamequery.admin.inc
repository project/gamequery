<?php

/**
 * @file
 * Administrative pages for GameQuery.
 */

/**
 * Page callback for admin/settings/gamequery.
 */
function gamequery_settings() {
  $form = array();

  // Find the GameQuery library.
  $path = gamequery_path();

  if (!$path) {
    drupal_set_message(t("You need to download the !library and place it in the %path folder of your server.", array('!library' => l(t('gameQuery library'), 'http://gamequery.onaluf.org/', array('attributes' => array('target' => '_blank'))), '%path' => 'sites/all/libraries')), 'error');
  }

  $form[gamequery_variable_name('path')] = array(
    '#type' => 'textfield',
    '#title' => t('Path to gameQuery library'),
    '#description' => t("You need to download the !library and place it in the %path folder of your server. The module will attempt to autodiscover its path, but you may override that by entering the path here.", array('!library' => l(t('gameQuery library'), 'http://gamequery.onaluf.org/', array('attributes' => array('target' => '_blank'))), '%path' => 'sites/all/libraries')),
    '#default_value' => $path,
  );

  return system_settings_form($form);
}
