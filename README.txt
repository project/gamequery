
GameQuery

GameQuery integrates the gameQuery jQuery game engine plugin with Drupal.

Installation instructions:

1) Install the module as usual.

2) Download the latest gamequery javascript somewhere in the sites/all/libaries
folder.

To invoke the plugin on a page, simply call gamequery_add_js().

@TODO:
* Add support for various sound wrapper libraries
    (see http://gamequery.onaluf.org/soundwrapper.php)
* Use Google repository if not found locally...

From http://gamequery.onaluf.org/

gameQuery is a jQuery plug-in to help make javascript game development easier
by adding some simple game-related classes. It's still in an early stage of
development and may change a lot in future versions. The project has a Google
Code page where the SVN repository of the project is hosted and a twitter page
where you can follow the daily progress of the development.
